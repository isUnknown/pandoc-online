package main

import (
    "fmt"
    "log"
    "net/http"
    "io/ioutil"
    "os/exec"
)

func helloHandler(w http.ResponseWriter, r *http.Request) {
    if r.URL.Path != "/hello" {
        http.Error(w, "404 not found.", http.StatusNotFound)
        return
    }

    if r.Method != "GET" {
        http.Error(w, "Method is not supported.", http.StatusNotFound)
        return
    }


    fmt.Fprintf(w, "Hello!")
}


func formHandler(w http.ResponseWriter, r *http.Request) {
    // Parse our multipart form, 10 << 20 specifies a maximum
    // upload of 10 MB files.

    if err := r.ParseMultipartForm(10 << 20); err != nil {
        fmt.Fprintf(w, "ParseForm() err: %v", err)
        return
    }
    fmt.Fprintf(w, "POST request successful\n")
    output_type := r.FormValue("output_type")
    file, handler, errFile := r.FormFile("file")
    if errFile != nil {
        fmt.Println("Error Retrieving the File")
        fmt.Println(errFile)
        return
    }

    fmt.Fprintf(w, "output_type = %s\n", output_type)

    defer file.Close()
    fmt.Printf("Uploaded File: %+v\n", handler.Filename)
    fmt.Printf("File Size: %+v\n", handler.Size)
    fmt.Printf("MIME Header: %+v\n", handler.Header)

   // Create a temporary file within our temp-images directory that follows
    // a particular naming pattern
    tempFile, err := ioutil.TempFile("temp_files", handler.Filename)
    if err != nil {
        fmt.Println(err)
    }
    defer tempFile.Close()
    fmt.Printf("Temp file filename: %+v\n", tempFile.Name())

    // read all of the contents of our uploaded file into a
    // byte array
    fileBytes, err := ioutil.ReadAll(file)
    if err != nil {
        fmt.Println(err)
    }
    // write this byte array to our temporary file
    tempFile.Write(fileBytes)
    // return that we have successfully uploaded our file!
    fmt.Fprintf(w, "Successfully Uploaded File\n")

    // exec pandoc command
    cmd := exec.Command("pandoc", "-s",  tempFile.Name(), "-o", "test.txt")

    err = cmd.Run()

    if err != nil {
        log.Fatal(err)
    }

    http.ServeFile(w, r, "test.txt")
}


func main() {
    fileServer := http.FileServer(http.Dir("./static")) // New code
    http.Handle("/", fileServer) // New code
    http.HandleFunc("/hello", helloHandler)
    http.HandleFunc("/form", formHandler)

    fmt.Printf("Starting server at port 9292\n")
    if err := http.ListenAndServe(":9292", nil); err != nil {
        log.Fatal(err)
    }
}

